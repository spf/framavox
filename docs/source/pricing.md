Foire Aux Questions
==========================

Notre groupe n'a pas (plus) de moyens financiers ! Pouvons-nous toujours continuer à utiliser Loomio ?
------------------------------------------------

Oui. Vous pouvez opter pour un /forfait/programme donation (voir notre page [Tarification](http://loomio.org/pricing)

Nous n'avons pas encore décidé du forfait qui nous conviendrait, pouvons-nous prolonger la période d'essai gratuite ?
-------------------------------------------------------------------------
Oui. Il suffit de faire la demande par courriel.


Comment fonctionne le plan donation ?
----------------------------

Si votre groupe choisi le plan donation, les membres verront une carte de ce type sur la page de leur groupe :

![Gift plan card](images/gift-plan-card.png)


Avez-vous des tarifs pour les souscriptions à l'année ?
------------------------------------------------

Oui. Notre offre standard est à 190 USD (Dollar US) par an (soit une économie de 38 USD par rapport au prix mensuel), et l'offre Plus est à 1790 USD par an (soit une économie de 358 USD)

Comment est intégré Slack ?
--------------------------------------
![Slack integration](images/slack-integration.png)

Grâce à l'intégration avec Slack, quand il y a une activité importante dans votre groupe Loomio (une nouvelle discussion ou proposition par exemple), elle peut être relayée sur le canal Slack de votre choix. Les gens peuvent voter directement depuis Slack en cliquant sur la position choisie. Notez, SVP, que cette intégration n'est disponible qu'avec des souscriptions payantes (Standard ou Plus).


Avec quelles cartes peut-on payer ?
------------------------------
Visa et Mastercard. Nous sommes en pourparlers avec American Express.

Êtes vous assujetti à la TVA ?
--------------------------------------

Oui. En Nouvelle-Zélande, la TVA est ajoutée au prix de chaque souscription payante. Il n'y a pas de TVA sur les donations.

Puis-je avoir une facture ?
-----------------------------------

Lors de votre première souscription vous recevrez un courriel de Chargify, notre système de paiement, avec pour objet **[Loomio\] Manage Your Subscription**. Vous y trouverez un lien pour vous connecter à notre portail de facturation où vous pourrez télécharger vos relevés.
