Framavox
---------

Framavox_ est un service en ligne de prise de décision proposé par l'association Framasoft.

Le service repose sur le logiciel libre Loomio_

Voici une documentation pour vous aider à l'utiliser.
Il s’agit de la traduction française de la `documentation officielle`_ réalisée par l'équipe Framalang_.
La traduction est encore incomplète et les captures d'écran sont en anglais pour le moment.

Qu'est-ce que Loomio?
---------------

|Loomio Logo|

Loomio_ est un outil de prise de décision collaborative simple et ergonomique. Loomio vous permet de mener des discussions en ligne, d'inviter les personnes susceptibles d'y participer, d'adopter des décisions opportunes et de traduire des délibérations en actions sur le monde réel.


Comment fonctionne Loomio
----------------

-   **Lancez un groupe** pour mener une activité collaborative avec d'autres sur Loomio ;

-   **Invitez des participants** à vous rejoindre dans votre groupe ;

-   **Lancez une discussion** sur ce qui vous passe par la tête ;

-   **Lancez une proposition** pour savoir comment chacun réagit à un projet particulier ;

-   **Décidez ensemble**  —  chacun peut approuver, désapprouver, s'abstenir, être en désaccord ou bloquer, ce qui vous permet de voir ce que chacun pense du projet proposé.

`Regardez cette courte vidéo`_ qui vous montre comment utiliser Loomio.


-----

.. _Framavox: https://framavox.org

.. _Loomio: https://loomio.org

.. _documentation officielle: https://loomio.gitbooks.io/manual/content/en/index.html

.. _Framalang: https://participer.framasoft.org/traduction-rejoignez-framalang/

.. |Loomio Logo| image:: images/logo.png

.. _Regardez cette courte vidéo: https://www.youtube.com/watch?v=RonxhKSSlG8&nohtml5=False qui vous montre comment utiliser Loomio.

.. toctree::
   :maxdepth: 1
   :caption: Table des matières
   :glob:

   getting_started.md
   group_settings.md
   coordinating_your_group.md
   inviting_new_members.md
   discussion_threads.md
   comments.md
   proposals.md
   subgroups.md
   reading_loomio.md
   keeping_up_to_date.md
   your_user_profile.md