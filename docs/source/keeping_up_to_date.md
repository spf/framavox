Se tenir à jour
==================

Paramètres des courriels
--------------
Les paramètres de vos e-mails contrôlent la fréquence à laquelle vous entendez parler d'un groupe ou d'un fil de discussion spécifique. Il y a trois paramètres pour chaque groupe et fil.


Vos paramètres de courriels vous permettent de définir la fréquence à laquelle vous avez des nouvelles d'un groupe ou d'un fil de discussion particulier. Il existe trois paramètres valables à la fois pour les groupes et les fils de discussion.

### Paramètres de la messagerie pour les groupes

![Notifications de groupe](images/edit_group_notifications.gif)

Pour modifier vos paramètres de courriel de groupe, affichez le menu déroulant **Options**, faites défiler et sélectionnez **Paramètres de courriel**.

* **Toute activité** : vous recevrez un courriel à chaque fois qu'il y a de l'activité (commentaires, votes, nouveaux fils, propositions, et résultats de proposition) dans ce groupe. **Toute activité** est le paramètre par défaut pour les nouveaux groupes. La section **Mettre à jour les paramètres de courriel pour tous les groupes** explique comment modifier les paramètres de courriel par défaut pour les nouveaux groupes.

-   **Activité importante** : vous recevrez un courriel au sujet des nouveaux fils et propositions (c'est-à-dire lorsqu'une proposition est créée, s'apprête à fermer ou quand un résultat est créé pour une proposition).

-   **Aucun courriel** : vous ne recevrez aucun courriel portant sur l'activité dans ce groupe, mais vous pourrez voir les dernières mises à jour sur les pages [**Non lues**]((#reading_loomio.html#unread-threads) et Récentes.

Pour appliquer les mêmes paramètres à l'ensemble de vos groupes, cochez la case **Appliquer ces paramètres à tous mes groupes** avant d'envoyer le formulaire.


![thread notifications](images/edit_thread_notifications.gif)


### Paramètres de la messagerie d'un fil de discussion

![Notifications liées à un fil](images/edit_thread_notifications.gif)

Lorsque vous commencez un nouveau fil de discussion, il hérite des paramètres de courriel par défaut attribués par son groupe. Pour modifier les paramètres de courriel, ouvrez le menu déroulant des options du fil et sélectionnez dans ce menu **Paramètres de courriel**.

-   **Toute activité** : vous recevrez un courriel à chaque fois qu'il y a de l'activité (commentaires, nouvelles propositions, votes et résultats d'une proposition) dans ce fil.

-   **Activité importante** : vous recevrez un courriel lorsqu'une proposition est créée, s'apprête à fermer ou lorsqu'un résultat est créé.

-   **Aucun courriel** : vous ne recevrez aucun courriel au sujet de ce fil.

Pour que les mêmes paramètres soient définis pour l'ensemble de vos fils de discussion, cochez la case **Appliquer ces paramètres à tous les fils de discussion dans ce groupe** avant d'envoyer le formulaire.


### Mettre à jour les paramètres de messagerie pour tous les groupes

![Régler (Configurer) les courriels](images/edit_user_notifications.gif)

Il y a trois paramètres de messagerie électronique supplémentaires qui peuvent être appliquées à tous les groupes.

**Messagerie, résumé quotidien** : l'activation de ce paramètre signifie que tous les matins vous recevrez un courriel résumant toutes les activités que vous avez manquées la veille. Cette messagerie fournit une routine simple pour rester à jour avec les activités Loomio.

**S'abonner… **: activer ce réglage signifie que, lorsque vous participez à un fil de discussion, toute nouvelle activité vous sera immédiatement envoyée par courriel.

**Avertir quand je suis mentionné** : activer ce réglage signifie que lorsque quelqu'un veut attirer votre attention il peut mentionner votre @nom dans un commentaire qui vous sera notifié. Nous recommandons de toujours activer ce paramètre, afin d'être informé par courriel dans ce cas.

Ces paramètres peuvent être configurés à partir des **paramètres de courriel** de la page utilisateur, qui est accessible par le menu **paramètres de courriel** dans le menu utilisateur.

### Paramètres par défaut des nouveaux groupes

Sous l'en-tête **Paramètres d'un groupe particulier** sur la page ** Paramètres de courriel** vous pouvez lire les paramètres de courriel par défaut pour les nouveaux groupes. Afin de modifier les paramètres de courriel par défaut pour tous les nouveaux groupes que vous rejoignez, cliquez sur le lien **Changer les paramètres par défaut** et sélectionnez les nouveaux paramètres par défaut.

### Fils muets

Lorsqu'un fil est muet, vous ne recevez plus de notifications sur les propositions, ou tout autre activité liée au fil de discussion, à moins que quelqu'un ne vous @mentionne.

Vous pouvez rendre les fils de discussion muets à partir de la prévisualisation des fils apparaissant sur la page du groupe, la page des fils récents et la page des fils non lus. Pour rendre un fil de discussion muet, survolez la prévisualisation du fil et sélectionnez l'option **muet**.

Les fils de discussion muets sont cachés sur les pages Fils récents et Fils non lus. Pour trouver vos fils de discussion muets, allez sur la page des Fils récents et sélectionnez « muet » dans le menu « Filtres des fils de discussion ».

### Marquer comme lu

Vous pouvez marquer comme lus les fils de discussion à partir de la prévisualisation des fils apparaissant sur la page du groupe, la page des fils récents et la page des fils non lus. Pour marquer comme lu un fil de discussion, survolez la prévisualisation du fil et sélectionnez l'option **Marqué comme lu**. Un fil de discussion marqué comme lu est enlevé de votre page [**Non lus**](reading_loomio.html#unread-threads).

### Répondre par courriel

Vous pouvez répondre à chaque commentaire que vous recevez directement par courriel et vos réponses seront postées sur le fil Loomio. Vous pouvez aussi choisir de voir le commentaire dans son contexte sur le fil de Loomio en cliquant sur le lien **Voir sur www.loomio.org** situé dans le bas du message.
